<?php

namespace Drupal\rsvplist\Form;

use Drupal\Component\Utility\EmailValidatorInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Database;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\user\UserStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an RSVP email form.
 */
class RSVPForm extends FormBase {

  /**
   * The path route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;
  /**
   * The path messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;
  /**
   * The path to email validator.
   *
   * @var \Drupal\Component\Utility\EmailValidatorInterface
   */
  private $emailValidator;
  /**
   * The user info.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  private $user;
  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  private $database;
  /**
   * The path to user info.
   *
   * @var \Drupal\user\UserStorageInterface
   */
  private $userStorage;

  /**
   * Constructs a RSVPForm object.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $routeMatch
   *   The path route match.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The path messenger.
   * @param \Drupal\Component\Utility\EmailValidatorInterface $emailValidator
   *   The email validator.
   * @param \Drupal\Core\Session\AccountProxyInterface $user
   *   The user info.
   * @param Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\user\UserStorageInterface $user_storage
   *   The user storage info.
   */
  public function __construct(RouteMatchInterface $routeMatch, MessengerInterface $messenger, EmailValidatorInterface $emailValidator, AccountProxyInterface $user, Connection $database, UserStorageInterface $user_storage) {
    $this->routeMatch = $routeMatch;
    $this->messenger = $messenger;
    $this->emailValidator = $emailValidator;
    $this->user = $user;
    $this->database = $database;
    $this->userStorage = $user_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_route_match'),
      $container->get('messenger'),
      $container->get('email.validator'),
      $container->get('current_user'),
      $container->get('database'),
      $container->get('entity_type.manager')->getStorage('user')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'rsvplist_email_form';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $node = $this->routeMatch->getParameter('node');
    if (isset($node)) {
      $nid = $node->id();
    }

    $form['email'] = [
      '#title' => $this->t('Email Address'),
      '#type' => 'textfield',
      '#size' => 25,
      '#description' => $this->t("We'll send updates to this email address."),
      '#required' => TRUE,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('RSVP'),
    ];

    $form['nid'] = [
      '#type' => 'hidden',
      '#value' => $nid,
    ];
    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $value = $form_state->getValue('email');
    if ($value == !$this->emailValidator->isValid($value)) {
      $form_state->setErrorByName('email', $this->t('The email address %mail is not valid.', ['%mail' => $value]));
      return;
    }

    $node = $this->routeMatch->getParameter('node');

    // Check if email already is set for this node.
    $select = Database::getConnection()->select('rsvplist', 'r');
    $select->fields('r', ['nid']);
    $select->condition('nid', $node->id());
    $select->condition('mail', $value);
    $results = $select->execute();
    if (!empty($results->fetchCol())) {
      // We found a row with this nid and email.
      $form_state->setErrorByName('email', $this->t('The address %mail is already subscribed to this list.', ['%mail' => $value]));
    }

  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $user = $this->userStorage->load($this->user->id());

    $this->database->insert('rsvplist')
      ->fields([
        'mail' => $form_state->getValue('email'),
        'nid' => $form_state->getValue('nid'),
        'uid' => $user->id(),
        'created' => time(),
      ])
      ->execute();

    $this->messenger->addMessage('Thank you for subscribing to the event');
  }

}
