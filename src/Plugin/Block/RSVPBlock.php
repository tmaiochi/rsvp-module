<?php

namespace Drupal\rsvplist\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\rsvplist\EnablerService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'RSVP' list block.
 *
 * @Block(
 *   id = "rsvp_block",
 *   admin_label = @Translation("RSVP Block"),
 *   category = @Translation("Blocks"),
 *  )
 */
class RSVPBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The path to FormBuilder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  private $formBuilder;
  /**
   * The path to RouteMatch.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  private $routeMatch;
  /**
   * The variable to my service.
   *
   * @var \Drupal\rsvplist\EnablerService
   */
  private $enabler;

  /**
   * Constructs the RSVP Block object.
   *
   * @param array $configuration
   *   The array configuration.
   * @param string $plugin_id
   *   The id for plugin.
   * @param mixed $plugin_definition
   *   The definition of plugin.
   * @param \Drupal\Core\Form\FormBuilderInterface $formBuilder
   *   The path to FormBuilder.
   * @param \Drupal\Core\Routing\RouteMatchInterface $routeMatch
   *   The path to RouteMatch.
   * @param \Drupal\rsvplist\EnablerService $enabler
   *   The enabler service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, FormBuilderInterface $formBuilder, RouteMatchInterface $routeMatch, EnablerService $enabler) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->formBuilder = $formBuilder;
    $this->routeMatch = $routeMatch;
    $this->enabler = $enabler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('form_builder'),
      $container->get('current_route_match'),
      $container->get('rsvplist.enabler'),
    );
  }

  /**
   * {@inheritDoc}
   */
  public function build() {
    return $this->formBuilder->getForm('Drupal\rsvplist\Form\RSVPForm');
  }

  /**
   * {@inheritDoc}
   */
  public function blockAccess(AccountInterface $account) {
    /** @var \Drupal\node\Entity\Node $node */
    $node = $this->routeMatch->getParameter('node');
    // $nid = null;
    /** @var \Drupal\rsvplist\EnablerService $enabler */
    $enabler = $this->enabler;
    if ($node) {
      if ($enabler->isEnabled($node)) {
        return AccessResult::allowedIfHasPermission($account, 'view rsvplist');
      }
    }

    return AccessResult::forbidden();
  }

}
