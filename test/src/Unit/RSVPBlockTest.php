<?php

namespace Drupal\Tests\rsvplist\Unit;

use Drupal\Component\DependencyInjection\Container;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\rsvplist\EnablerService;
use Drupal\rsvplist\Plugin\Block\RSVPBlock;
use Drupal\Tests\UnitTestCase;

/**
 * Unit Test for Block.
 */
class RSVPBlockTest extends UnitTestCase {

  /**
   * Provides a container optimized for Drupal's needs.
   *
   * @var \Drupal\Component\DependencyInjection\Container
   */
  protected $container;

  /**
   * {@inheritDoc}
   */
  protected function setUp(): void {
    $container = new Container();
    $cache_context_manager = $this->getMockBuilder('Drupal\Core\Cache\Context\CacheContextsManager')
      ->disableOriginalConstructor()
      ->onlyMethods(['assertValidTokens'])
      ->getMock();
    $container->set('cache_contexts_manager', $cache_context_manager);
    $cache_context_manager->expects($this->any())
      ->method('assertValidTokens')
      ->willReturn(TRUE);
    \Drupal::setContainer($container);

    $this->form_builder = $this->getMockBuilder(FormBuilderInterface::class)
      ->disableOriginalConstructor()
      ->getMock();

    $this->node = $this->getMockBuilder('\Drupal\node\Entity\Node')
      ->disableOriginalConstructor()
      ->getMock();

    $this->routeMatch = $this->prophesize(RouteMatchInterface::class);
    $this->routeMatch->getParameter('node')->willReturn($this->node);

    $this->rsvplist_enabler = $this->getMockBuilder(EnablerService::class)
      ->disableOriginalConstructor()
      ->getMock();

    $this->rsvplist_enabler->expects($this->any())
      ->method('isEnabled')
      ->will($this->returnValue(TRUE));

    $this->blockAccess = new RSVPBlock([], '', ['provider' => 'unit_test'],
      $this->form_builder, $this->routeMatch->reveal(), $this->rsvplist_enabler);

  }

  /**
   * Tests the blockAccess() method.
   */
  public function testBlockAccess() {
    $this->account = $this->createMock('Drupal\Core\Session\AccountInterface');
    $this->assertEquals(AccessResult::allowedIfHasPermission(
      $this->account, 'view rsvplist'),
      $this->blockAccess->blockAccess($this->account)
    );
  }

}
