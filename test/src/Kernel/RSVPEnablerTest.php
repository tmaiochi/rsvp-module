<?php

namespace Drupal\Tests\rsvplist\Kernel;

use Drupal\Core\Database\Database;
use Drupal\KernelTests\KernelTestBase;
use Drupal\node\Entity\Node;
use Drupal\rsvplist\EnablerService;

/**
 * Kernel test class.
 */
class RSVPEnablerTest extends KernelTestBase {

  /**
   * {@inheritDoc}
   */
  protected static $modules = [
    'rsvplist', // Load our own module, so we can access and test it.
    'node', // Load the node module, as we use it as a parameter on the tests.
    'user', // Load the user module, because we need one to verify the access.
  ];

  /**
   * {@inheritDoc}
   */
  public function setUp(): void {
    parent::setUp();
    $this->installSchema('rsvplist', 'rsvplist_enabled', 'system');
    $this->installEntitySchema('user');
    $this->installEntitySchema('node');
  }

  /**
   * Tests the Enabler::setEnabler and Enabler::delEnabler method.
   */
  public function testDelEnabler() {
    $node1 = Node::create([
      'type' => 'article',
      'title' => 'test',
    ]);
    $node1->save();

    $enabler = new EnablerService();
    $enabler->setEnabled($node1);
    $select = Database::getConnection()->select('rsvplist_enabled', 're');
    $select->fields('re', ['nid']);
    $select->condition('nid', $node1->id());
    $results = $select->execute()->fetchCol();
    $this->assertNotEmpty($results);

    $enabler->delEnabled($node1);
    $select = Database::getConnection()->select('rsvplist_enabled', 're');
    $select->fields('re', ['nid']);
    $select->condition('nid', $node1->id());
    $results = $select->execute()->fetchCol();
    $this->assertEmpty($results);

  }

}
