<?php

namespace Drupal\Tests\rsvplist\Functional;

use Drupal\node\Entity\Node;
use Drupal\Tests\BrowserTestBase;

/**
 * Functional test for RSVP.
 */
class RSVPTest extends BrowserTestBase {

  /**
   * {@inheritDoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritDoc}
   */
  protected static $modules = [
    'node',
    'user',
    'rsvplist',
    'system',
    'block',
  ];

  /**
   * {@inheritDoc}
   */
  public function setUp(): void {
    parent::setUp();

    // Create user who will be used in this test with the view rsvp permission.
    $this->user1 = $this->drupalCreateUser(['view rsvplist']);
    $this->drupalCreateContentType([
      'type' => 'article',
      'name' => 'Article',
    ]);

    // Create a admin user with all permissions.
    $this->adminUser = $this->drupalCreateUser([
      'administer site configuration',
      'administer blocks',
      'create article content',
      'view rsvplist',
      'administer rsvplist',
    ]);
  }

  /**
   * Test access to the RSVP reports.
   */
  public function testProfileAccess() {
    $this->drupalGet('admin/reports/rsvplist');
    $this->assertSession()->statusCodeEquals(403);

    $this->drupalLogin($this->user1);
    $this->drupalGet('admin/reports/rsvplist');
    $this->assertSession()->statusCodeEquals(200);

  }

  /**
   * Test all functionalities from RSVP.
   */
  public function testAllFunctionalities() {
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('admin/config/content/rsvplist');
    $this->assertSession()->statusCodeEquals(200);
    $this->submitForm(['rsvplist_types[article]' => TRUE], 'Save configuration');

    $this->assertSession()->pageTextContains('The configuration options have been saved');
    $this->drupalPlaceBlock('rsvp_block', [
      'id' => 'rsvpblock',
      'label' => 'RSVP Block',
    ]);

    $this->drupalGet('admin/structure/block/manage/rsvpblock');
    $this->submitForm(['visibility[entity_bundle:node][bundles][article]' => TRUE], 'Save block');

    $this->drupalGet('node/add/article');
    $this->assertSession()->pageTextContains('RSVP COLLECTION');
    $this->submitForm([
      'title[0][value]' => 'test',
      'edit-rsvplist-enabled' => TRUE,
    ], 'Save');

    $this->drupalGet('node/1');
    $this->assertSession()->pageTextContains('RSVP Block');
    $this->assertSession()->pageTextContains('Email Address');
    $this->assertSession()->pageTextContains("We'll send updates to this email address.");

    $this->submitForm(['edit-email' => 'test@test.com'], 'RSVP');
    $this->drupalGet('/admin/reports/rsvplist');
    $this->assertSession()->pageTextContains('test@test.com');
  }

  /**
   * Another way to make the test bellow.
   */
  public function testRsvp() {
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('admin/config/content/rsvplist');
    $this->assertSession()->statusCodeEquals(200);
    $this->submitForm(['rsvplist_types[article]' => TRUE], 'Save configuration');

    $this->assertSession()->pageTextContains('The configuration options have been saved');
    $this->drupalPlaceBlock('rsvp_block', [
      'id' => 'rsvpblock',
      'label' => 'RSVP Block',
    ]);

    $this->drupalGet('admin/structure/block/manage/rsvpblock');
    $this->submitForm(['visibility[entity_bundle:node][bundles][article]' => TRUE], 'Save block');

    $node = Node::create([
      'type' => 'article',
      'title' => 'test',
    ]);
    $node->save();

    \Drupal::service('rsvplist.enabler')->setEnabled($node);

    $this->drupalGet('node/' . $node->id());
    $this->assertSession()->pageTextContains('RSVP Block');
    $this->assertSession()->pageTextContains('Email Address');
    $this->assertSession()->pageTextContains("We'll send updates to this email address.");

    $this->submitForm(['edit-email' => 'test@test.com'], 'RSVP');
    $this->drupalGet('/admin/reports/rsvplist');
    $this->assertSession()->pageTextContains('test@test.com');
  }

}
